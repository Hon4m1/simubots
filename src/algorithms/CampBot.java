/* ******************************************************
 * Simovies - Eurobot 2015 Robomovies Simulator.
 * Copyright (C) 2014 <Binh-Minh.Bui-Xuan@ens-lyon.org>.
 * GPL version>=3 <http://www.gnu.org/licenses/>.
 * $Id: algorithms/CampBot.java 2014-11-04 buixuan.
 * ******************************************************/
package algorithms;

import robotsimulator.Brain;
import characteristics.IFrontSensorResult;
import characteristics.Parameters;
import characteristics.IRadarResult;
import robotsimulator.RadarResult;

import static characteristics.IRadarResult.Types.OpponentMainBot;
import static characteristics.IRadarResult.Types.OpponentSecondaryBot;


public class CampBot extends Brain {
  private static final double ANGLEPRECISION = 0.01;
  private boolean turnTask,turnRight,finished,taskOne;
  private double endTaskDirection;
  private int endTaskCounter;
  private static IFrontSensorResult.Types WALL=IFrontSensorResult.Types.WALL;
  private static IFrontSensorResult.Types TEAMMAIN=IFrontSensorResult.Types.TeamMainBot;
  private double oldAngle;

  public CampBot() { super(); }

  public void activate() {
    turnTask=true;
    finished=false;
    taskOne=true;
    oldAngle=myGetHeading();
    endTaskDirection=getHeading()+0.4*Math.PI;
    sendLogMessage("Moving and healthy.");
  }
  public void step() {
    if (getHealth()<=0) { sendLogMessage("I'm dead.");return; }
    if (finished) { sendLogMessage("Camping point. Task complete.");return; }
    if (turnTask) {

      if (isHeading(endTaskDirection)) {
        turnTask=false;
        if (taskOne) endTaskCounter=10; else endTaskCounter=100;
        move();
      } else {
        move();
        double max = 450;
        IRadarResult target = null;
        for (IRadarResult o: detectRadar()) {
          if(o.getObjectType()==OpponentSecondaryBot && o.getObjectDistance()<=max){
            target = o;
          }
          else{
            target = o;
          }
        }
        if(!isSameDirection(myGetHeading(),target.getObjectDirection())){
          stepTurn(Parameters.Direction.RIGHT);
          System.out.println("radar : " + target +" distance : "+target.getObjectDistance());
        }
        move();
      }
    }
    if (endTaskCounter>0) {
      endTaskCounter--;
      move();
      return;
    } else {
      taskOne=false;
      if (detectFront().getObjectType()==WALL||detectFront().getObjectType()==TEAMMAIN)
      {
        stepTurn(Parameters.Direction.RIGHT);
        move();
      }

      turnTask=true;
      endTaskDirection=getHeading()+0.5*Math.PI;
      return;
    }
  }
  private boolean isHeading(double dir){
    return Math.abs(Math.sin(getHeading()-dir))<Parameters.teamBSecondaryBotStepTurnAngle;
  }
  private double normalizeRadian(double angle){
    double result = angle;
    while(result<0) result+=2*Math.PI;
    while(result>=2*Math.PI) result-=2*Math.PI;
    return result;
  }
  private boolean isSameDirection(double dir1, double dir2){
    return Math.abs(normalizeRadian(dir1)-normalizeRadian(dir2))<ANGLEPRECISION;
  }
  private double myGetHeading(){
    return normalizeRadian(getHeading());
  }
}
